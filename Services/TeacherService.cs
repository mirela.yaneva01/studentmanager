﻿using School.Dtos;
using School.Dtos.Students;
using School.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Services
{
    class TeacherService
    {
        private TeacherRepository teacherRepo = new TeacherRepository();

        public void Create(Teacher dto)
        {
            dto.Id = Guid.NewGuid();
            teacherRepo.Create(dto);
        }

        public TeacherInfoDto Read(Guid id)
        {
            var dto = teacherRepo.GetByID(id);
            TeacherInfoDto infoDto = new TeacherInfoDto();
            infoDto.Salary = dto.DailySalary * 22;
            infoDto.Teacher = dto;
            return infoDto;
        }

        public void Update(Teacher dto)
        {
            teacherRepo.Update(dto);
        }

        public void Delete(Guid id)
        {
            teacherRepo.Delete(id);
        }

        public List<NameAndIdDto> GetAll()
        {
            var studentDtos = teacherRepo.GetAll().Select(x => new NameAndIdDto { Name = x.Name, Id = x.Id });
            return studentDtos.ToList();
        }

        public void AddDailySalary(Guid id, decimal dailySalary)
        {
            var userInputId = teacherRepo.GetByID(id);
            userInputId.DailySalary = dailySalary;
            teacherRepo.Update(userInputId);
        }
    }
}
