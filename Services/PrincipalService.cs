﻿using School.Dtos.Students;
using School.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Services
{
    class PrincipalService
    {
        private PrincipalRepository repoPrincical = new PrincipalRepository();

        public bool Create(Principal dto)
        {
            if (repoPrincical.GetAll().Count < 1)
            {
                dto.Id = Guid.NewGuid();
                repoPrincical.Create(dto);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Principal Read(Guid id)
        {
            var dto = repoPrincical.GetByID(id);
            return dto;
        }

        public void Delete(Guid id)
        {
            repoPrincical.Delete(id);
        }
    }
}
