﻿using School.Dtos;
using School.Dtos.Students;
using School.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Services
{
    class StudentService
    {
        private StudentRepository studentRepo = new StudentRepository();

        public void Create(Student dto)
        {
            dto.Id = Guid.NewGuid();
            studentRepo.Create(dto);
        }

        public StudentInfoDto Read(Guid id)
        {
            var dto = studentRepo.GetByID(id);
            StudentInfoDto infoDto = new StudentInfoDto();
            if (dto.Grades.Count > 0)
            {
                infoDto.AverageGrade = dto.Grades.Average();
            }
            else
            {
                infoDto.AverageGrade = 0;
            }
            infoDto.Student = dto;
            return infoDto;
        }

        public List<NameAndIdDto> GetAll()
        {
            var studentDtos = studentRepo.GetAll().Select(x => new NameAndIdDto {Name = x.Name, Id = x.Id});
            return studentDtos.ToList();
        }

       public void Update(Student dto)
        {
            studentRepo.Update(dto);
        }

        public void Delete(Guid id)
        {
            studentRepo.Delete(id);
        }

        public void AddGrade(Guid id, int grade)
        {
            var userInputId = studentRepo.GetByID(id);
            userInputId.Grades.Add(grade);
            studentRepo.Update(userInputId);
        }
    }
}
