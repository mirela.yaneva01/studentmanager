﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entitis
{
    class StudentEntity : UserEntity
    {
        public List<GradesEntity> Grades { get; set; }

        public Guid TeacherId { get; set; }
        public TeacherEntity Teacher { get; set; }
    }
}
