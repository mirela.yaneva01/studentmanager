﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entitis
{
    class TeacherEntity : UserEntity
    {
        public decimal DailySalary { get; set; }

        public List<StudentEntity> Students { get; set; }
    }
}
