﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Entitis
{
    class GradesEntity
    {
        public int Grade { get; set; }

        public StudentEntity Student { get; set; }
        public Guid StudentId { get; set; }
        public Guid Id { get; set; }
    }
}
