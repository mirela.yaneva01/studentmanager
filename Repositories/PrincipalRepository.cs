﻿using School.Dtos.Students;
using School.Entitis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Repositories
{
    class PrincipalRepository : BaseRepository<Principal, PrincipalEntity>
    {
        protected override PrincipalEntity MapDTOToEntity(Principal dto)
        {
            PrincipalEntity entity = new PrincipalEntity();
            MapDTOToEntity(dto, entity);
            return entity;
        }

        protected override void MapDTOToEntity(Principal dto, PrincipalEntity entity)
        {
            entity.Name = dto.Name;
            entity.BirthDate = dto.BirthDate;
            entity.Password = dto.Password;
            entity.UserName = dto.UserName;
            entity.SchoolName = dto.SchoolName;
            entity.Id = dto.Id;
        }

        protected override Principal MapEntityToDTO(PrincipalEntity entity)
        {
            Principal dto = new Principal();
            dto.Name = entity.Name;
            dto.BirthDate = entity.BirthDate;
            dto.Password = entity.Password;
            dto.UserName = entity.UserName;
            dto.SchoolName = entity.SchoolName;
            dto.Id = entity.Id;
            return dto;
        }
    }
}
