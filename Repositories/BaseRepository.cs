﻿using Microsoft.EntityFrameworkCore;
using School.Dtos.Students;
using School.Entitis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Repositories
{
   abstract class BaseRepository<T, TEntity>
        where T : User
        where TEntity : UserEntity
    {
        protected readonly SchoolContext context;
        private DbSet<TEntity> entities;
        public BaseRepository()
        {
            this.context = new SchoolContext();
            entities = context.Set<TEntity>();
        }
        public void Create(T dto)
        {
            entities.Add(MapDTOToEntity(dto));
            context.SaveChanges();
        }

        public List<T> GetAll()
        {
            var result = new List<T>();
            foreach (var entity in entities)
            {
                var dto = MapEntityToDTO(entity);

                result.Add(dto);
            }
            return result;
        }

        public T GetByID(Guid id)
        {
            var entity = entities.FirstOrDefault(x => x.Id == id);
            var dto = MapEntityToDTO(entity); 
            return dto;
        }
       
        public void Update(T dto)
        {
            var entity = entities.FirstOrDefault(x => x.Id == dto.Id);

            MapDTOToEntity(dto, entity);
            entities.Update(entity);
            context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = entities.FirstOrDefault(x => x.Id == id);
            entities.Remove(entity);
            context.SaveChanges();
        }
        protected abstract T MapEntityToDTO(TEntity entity);

        protected abstract TEntity MapDTOToEntity(T dto);

        protected abstract void MapDTOToEntity(T dto, TEntity entity);


    }
}
