﻿using School.Dtos.Students;
using School.Entitis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Repositories
{
    class TeacherRepository : BaseRepository<Teacher, TeacherEntity>
    {
        protected override TeacherEntity MapDTOToEntity(Teacher dto)
        {
            TeacherEntity entity = new TeacherEntity();
            MapDTOToEntity(dto, entity);
            return entity;
        }

        protected override void MapDTOToEntity(Teacher dto, TeacherEntity entity)
        {
            entity.Name = dto.Name;
            entity.BirthDate = dto.BirthDate;
            entity.Password = dto.Password;
            entity.UserName = dto.UserName;
            entity.DailySalary = dto.DailySalary;
            entity.Id = dto.Id;
        }

        protected override Teacher MapEntityToDTO(TeacherEntity entity)
        {
            Teacher dto = new Teacher();
            dto.Name = entity.Name;
            dto.BirthDate = entity.BirthDate;
            dto.Password = entity.Password;
            dto.UserName = entity.UserName;
            dto.DailySalary = entity.DailySalary;
            dto.Id = entity.Id;
            return dto;
        }
    }
}
