﻿using School.Dtos.Students;
using School.Entitis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Repositories
{
    class StudentRepository : BaseRepository<Student, StudentEntity>
    {
        protected override StudentEntity MapDTOToEntity(Student dto)
        {
            StudentEntity entity = new StudentEntity();
            MapDTOToEntity(dto, entity);
            return entity;
        }

        protected override void MapDTOToEntity(Student dto, StudentEntity entity)
        {
            entity.Name = dto.Name;
            entity.BirthDate = dto.BirthDate;
            entity.Grades = dto.Grades.Select(x => new GradesEntity {  Student = entity, StudentId = dto.Id, Grade = x}).ToList();
            entity.Password = dto.Password;
            entity.UserName = dto.UserName;
            entity.TeacherId = dto.TeacherId;
            entity.Id = dto.Id;
        }

        protected override Student MapEntityToDTO(StudentEntity entity)
        {
            Student dto = new Student();
            dto.Name = entity.Name;
            dto.BirthDate = entity.BirthDate;
            dto.Grades.AddRange(entity.Grades.Select(x => x.Grade));
            dto.Password = entity.Password;
            dto.UserName = entity.UserName;
            dto.TeacherId = entity.TeacherId;
            dto.Id = entity.Id;
            return dto;
        }
    }
}
