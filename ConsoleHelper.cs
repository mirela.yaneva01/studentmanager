﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    class ConsoleHelper
    {
        public static string Ask(string question)
        {
            System.Console.WriteLine(question);
            var answer = GetValidString();
            return answer;
        }

        public static string Ask(DateTime question)
        {
            return Ask(question.ToString());
        }

        public static string Ask(string question, DateTime date)
        {
            return Ask($"[{date}] {question}");
        }

        public static void PrintErrorMessage(string error)
        {
            System.Console.BackgroundColor = ConsoleColor.Red;
            System.Console.WriteLine(error);
            System.Console.ResetColor();
        }

        private static string GetValidString()
        {
            string newString = System.Console.ReadLine();

            while (newString == string.Empty)
            {
                PrintErrorMessage("Empty input!!");
                newString = System.Console.ReadLine();
            }

            return newString;
        }
    }
}
