﻿using Microsoft.EntityFrameworkCore;
using School.Entitis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    class SchoolContext : DbContext
    {
        public DbSet <StudentEntity> Studens { get; set; }
        public DbSet <TeacherEntity> Teachers { get; set; }
        public DbSet <PrincipalEntity> Principal { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=SchoolDB;Trusted_Connection=True;");
        }
    }
}
