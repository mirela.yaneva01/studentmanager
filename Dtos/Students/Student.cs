﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Dtos.Students
{
    class Student : User
    {
        public List<int> Grades { get; }

        public Guid TeacherId { get; set; }

        public Student()
        {
            Grades = new List<int>();
        }
    }
}
