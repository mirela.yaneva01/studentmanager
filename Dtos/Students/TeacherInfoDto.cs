﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Dtos.Students
{
    class TeacherInfoDto
    {
        public Teacher Teacher { get; set; }

        public decimal Salary { get; set; }

    }
}
