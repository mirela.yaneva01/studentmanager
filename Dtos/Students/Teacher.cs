﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Dtos.Students
{
    class Teacher : User
    {
        public decimal DailySalary { get; set; }

        public List<Guid> StudentIds { get; set; }
    }
}
