﻿using School.Controllers;
using School.Dtos.Students;
using School.Services;
using System;
using System.Collections.Generic;

namespace School
{
    class Program
    {
        static void Main(string[] args)
        {
           

            while (true)
            {
                Console.WriteLine("[1] Student Management");
                Console.WriteLine("[2] Teacher Management");
                Console.WriteLine("[3] Principal Management");
                string userInput = ConsoleHelper.Ask("[X] Exit");

                if (userInput == "1")
                {
                    StudentController.ShowStudentView();
                }
                else if(userInput == "2")
                {
                    TeacherController.ShowTeacherView();
                }
                else if(userInput == "3")
                {
                    PrincipalController.ShowPrincipalView();
                }
                else if(userInput == "X")
                {
                    break;
                }
            }
        }
    }
}
