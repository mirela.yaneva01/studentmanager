﻿using School.Dtos.Students;
using School.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Controllers
{
    public static class StudentController
    {
        static StudentService studentService = new StudentService();

        public static void ShowStudentView()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("[1] Create Student");
                Console.WriteLine("[2] Read Student");
                Console.WriteLine("[3] Update Student");
                Console.WriteLine("[4] Delete Student");
                Console.WriteLine("[5] Read all Students");
                Console.WriteLine("[6] Add Grade");
                string userInputStudents = ConsoleHelper.Ask("[X] Return");

                Console.Clear();
                if (userInputStudents == "1")
                {
                    var createStudent = new Student();
                    PopulateStudent(createStudent);
                    studentService.Create(createStudent);
                    Console.WriteLine("You added new student!");
                }
                else if (userInputStudents == "2")
                {
                    var userInputId = ConsoleHelper.Ask("Enter students id:");
                    var infoDto =  studentService.Read(Guid.Parse(userInputId));
                    Console.WriteLine($"Name: {infoDto.Student.Name}");
                    Console.WriteLine($"Username : {infoDto.Student.UserName}");
                    Console.WriteLine($"Birthdate: {infoDto.Student.BirthDate}");
                    Console.WriteLine($"Average Grade: {infoDto.AverageGrade}");
                }
                else if (userInputStudents == "3")
                {
                    var userInputId = ConsoleHelper.Ask("Enter students id:");
                    var updateDto = new Student();
                    updateDto.Id = Guid.Parse(userInputId);
                    PopulateStudent(updateDto);
                    studentService.Update(updateDto);
                    Console.WriteLine("You updated a student!");
                }
                else if(userInputStudents == "4")
                {
                    var userInputId = ConsoleHelper.Ask("Enter students id:");
                    studentService.Delete(Guid.Parse(userInputStudents));
                }
                else if (userInputStudents == "5")
                {
                    var allStudents = studentService.GetAll();
                    for (int i = 0; i < allStudents.Count; i++)
                    {
                        Console.WriteLine($"Name: {allStudents[i].Name}, ID: {allStudents[i].Id}");
                    }
                }
                else if(userInputStudents == "6")
                {
                    var userInputId = ConsoleHelper.Ask("Enter students id:");
                    var userInputGrade = ConsoleHelper.Ask("Add new grade:");
                    studentService.AddGrade(Guid.Parse(userInputId), int.Parse(userInputGrade));
                    Console.WriteLine("You added a new grade!");
                }
                else if (userInputStudents.ToUpper() == "X")
                {
                    break;
                }
                Console.ReadKey(true);
            }
        }

        static void PopulateStudent(Student student)
        {
            student.Name = ConsoleHelper.Ask("Add students name:");
            student.BirthDate = DateTime.Parse(ConsoleHelper.Ask("Add birth date:"));
            student.UserName = ConsoleHelper.Ask("Add username:");
            student.Password = ConsoleHelper.Ask("Add password:");
            student.TeacherId = Guid.Parse(ConsoleHelper.Ask("Add teachers ID:"));
        }
    }
}
