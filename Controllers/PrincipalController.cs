﻿using School.Dtos.Students;
using School.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Controllers
{
    public static class PrincipalController
    {
        static PrincipalService principalService = new PrincipalService();

        public static void ShowPrincipalView()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("[1] Add principal:");
                Console.WriteLine("[2] Read principal:");
                Console.WriteLine("[3] Delete principal:");
                string userInputPrincipal = ConsoleHelper.Ask("[X] Return");

                if (userInputPrincipal == "1")
                {
                    var createPrincipal = new Principal();
                    PopulatePrincipal(createPrincipal);

                    if (principalService.Create(createPrincipal))
                    {
                        Console.WriteLine($"You added principal with ID: {createPrincipal.Id}!");
                    }
                    else
                    {
                        ConsoleHelper.PrintErrorMessage("You can only have one principal!!!");
                    }
                }
                else if(userInputPrincipal == "2")
                {
                    var userInput = ConsoleHelper.Ask("Enter principal ID:");
                    var infoDto = principalService.Read(Guid.Parse(userInput));
                    Console.WriteLine($"Name: {infoDto.Name}");
                    Console.WriteLine($"Birthdate: {infoDto.BirthDate}");
                    Console.WriteLine($"Username: {infoDto.UserName}");
                    Console.WriteLine($"School name: {infoDto.SchoolName}");
                }
                else if(userInputPrincipal == "3")
                {
                    var userInput = ConsoleHelper.Ask("Enter principal ID:");
                    principalService.Delete(Guid.Parse(userInput));
                }
                else if (userInputPrincipal.ToUpper() == "X")
                {
                    break;
                }
                Console.ReadKey(true);
            }
        }

        static void PopulatePrincipal(Principal principal)
        {
            principal.Name = ConsoleHelper.Ask("Add principals name:");
            principal.BirthDate = DateTime.Parse(ConsoleHelper.Ask("Add birthdate:"));
            principal.UserName = ConsoleHelper.Ask("Add principals username:");
            principal.Password = ConsoleHelper.Ask("Add password:");
            principal.SchoolName = ConsoleHelper.Ask("Add school name:");
        }
    }
}
