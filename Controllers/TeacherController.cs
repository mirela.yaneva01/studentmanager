﻿using School.Dtos.Students;
using School.Services;
using System;

namespace School.Controllers
{
    public static class TeacherController
    {
        static TeacherService teacherService = new TeacherService();

        public static void ShowTeacherView()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("[1] Create teacher");
                Console.WriteLine("[2] Read teacher");
                Console.WriteLine("[3] Update teacher");
                Console.WriteLine("[4] Delete teacher");
                Console.WriteLine("[5] Read all teachers");
                Console.WriteLine("[6] Add daily salary");
                string userInputTeachers = ConsoleHelper.Ask("[X] Return");

                Console.Clear();

                if (userInputTeachers == "1")
                {
                    var createTeacher = new Teacher();
                    PopulateTeacher(createTeacher);
                    teacherService.Create(createTeacher);
                    Console.WriteLine("You added new teacher!");
                }
                else if(userInputTeachers == "2")
                {
                    var userInput = ConsoleHelper.Ask("Add teachers ID:");
                    var infoDto = teacherService.Read(Guid.Parse(userInput));
                    Console.WriteLine($"Name: {infoDto.Teacher.Name}");
                    Console.WriteLine($"Birthdate: {infoDto.Teacher.BirthDate}");
                    Console.WriteLine($"Username: {infoDto.Teacher.UserName}");
                    Console.WriteLine($"Salary: {infoDto.Salary}");
                }
                else if(userInputTeachers == "3")
                {
                    var userInput = ConsoleHelper.Ask("Add teachers ID:");
                    var inputDto = new Teacher();
                    inputDto.Id = Guid.Parse(userInput);
                    PopulateTeacher(inputDto);
                    teacherService.Update(inputDto);
                    Console.WriteLine("You updated a teacher!");
                }
                else if(userInputTeachers == "4")
                {
                    var userInput = ConsoleHelper.Ask("Add teachers ID:");
                    teacherService.Delete(Guid.Parse(userInput));
                }
                else if(userInputTeachers == "5")
                {
                    var allTeachers = teacherService.GetAll();
                    for (int i = 0; i < allTeachers.Count; i++)
                    {
                        Console.WriteLine($"Name: {allTeachers[i].Name}, ID: {allTeachers[i].Id}");
                    }
                }
                else if(userInputTeachers == "6")
                {
                    var userInput = ConsoleHelper.Ask("Add teachers ID:");
                    var userInputSalary = ConsoleHelper.Ask("Add daily salary:");
                    teacherService.AddDailySalary(Guid.Parse(userInput), decimal.Parse(userInputSalary));
                    Console.WriteLine("You added daily salary!");
                }
                else if (userInputTeachers.ToUpper() == "X")
                {
                    break;
                }
                Console.ReadKey(true);
            }
        }

       static void PopulateTeacher(Teacher teacher)
        {
            teacher.Name = ConsoleHelper.Ask("Add teachers name:");
            teacher.BirthDate = DateTime.Parse(ConsoleHelper.Ask("Add birthdate:"));
            teacher.UserName = ConsoleHelper.Ask("Add teachers username:");
            teacher.Password = ConsoleHelper.Ask("Add password:");
        }
    }

}
